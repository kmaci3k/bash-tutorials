#!/bin/bash

# Zakoncz program gdy aktualnie wykonywana instrukcja zwrocila kod rozny od 0
set -e
# set -x

exec 3>&1;

# funkcja ktorej zadaniem jest wyswietlenie menu oraz zwrocenie informacji, ktora opcja zostala wybrana
function showMenu {
	# Wyswietl menu z dostepnymi funkcjami
	menuResult=$(dialog --menu "Jaką funkcję chcesz przetestować ?" 15 50 4 \
		1 "Wyszukaj -> grep" \
		2 "Sortuj -> sort" \
		3 "Zamien -> sed" \
		4 "Wyjście" 2>&1 1>&3);
	# Zwroc z funkcji wybrana opcje menu
	eval $1="'$menuResult'"
}

# Funkcja ktorej zadaniem jest wyszukanie wpisanego tekstu przy pomocy funkcji grep
function grepOption {
	# Wczytaj dane wejsciowe z pliku lakes.in
	local inputLakes=$(cat lakes.in)
	# Wyswietl zawartosc pliku lakes.in
	dialog --clear --title "Dane wejściowe (plik: lakes.in)" --msgbox "$inputLakes" 20 40

	# Wczytaj fraze do wyszukiwania
	searchPhrase=$(dialog --title "Wyszukaj" --inputbox "Podaj fraze do wyszukania" 8 40 2>&1 1>&3)

	# Wykonaj wyszukiwanie, a wynik zapisz do zmiennej lokalnej
	local outputLakes=$(cat lakes.in | grep $searchPhrase)
	# Zapisz wynik do pliku lakes.out
	echo "$outputLakes" > lakes.out

	# Wyswietl wynik wyszukiwania
	dialog --clear --title "Dane wyjściowe (plik: lakes.out)" --msgbox "$outputLakes" 20 40
}

# Funkcja ktorej zadaniem jest sortowanie zawartosci pliku
function sortOption {
	# Wczytaj dane wejsciowe z pliku lakes.in
	local inputLakes=$(cat lakes.in)
	# Wyswietl zawartosc pliku lakes.in
	dialog --clear --title "Dane wejściowe (plik: lakes.in)" --msgbox "$inputLakes" 20 40

	# Wykonaj sortowanie
	local outputLakes=$(sort lakes.in)
	# Zapisz wynik do pliku lakes.out
	echo "$outputLakes" > lakes.out

	# Wyswietl wynik sortowania
	dialog --clear --title "Dane wyjściowe (plik: lakes.out)" --msgbox "$outputLakes" 20 40
}

# Funkcje ktorej zadaniem jest zamiana wyszukiwanej wartosci na inna (podana przez uzytkownika)
function sedOption {
	# Wczytaj dane wejsciowe z pliku lakes.in
	local inputLakes=$(cat lakes.in)
	# Wyswietl zawartosc pliku lakes.in
	dialog --clear --title "Dane wejściowe (plik: lakes.in)" --msgbox "$inputLakes" 20 40

	# Wczytaj fraze do zamiany
	searchPhrase=$(dialog --title "Wyszukaj" --inputbox "Podaj fraze do wyszukania:" 8 40 2>&1 1>&3)
	# Wczytaj fraze, ktora ma zastapic fraze wpisana powyzej
	replacePhrase=$(dialog --title "Zamień" --inputbox "Zamień na:" 8 40 2>&1 1>&3)

	# Wykonaj zamiana, a wynik zapisz do zmiennej lokalnej
	local outputLakes=$(cat lakes.in | sed s/$searchPhrase/$replacePhrase/g)
	# Zapisz wynik do pliku lakes.out
	echo "$outputLakes" > lakes.out

	# Wyswietl wynik wyszukiwania
	dialog --clear --title "Dane wyjściowe (plik: lakes.out)" --msgbox "$outputLakes" 20 40
}

# Wyswietl menu
showMenu menuOption
# Wyswietlaj menu tak dlugo az uzytkownik wybierze opcje wyjscie lub wybierze 'Cancel'
while [ $menuOption != 4 ]
 do
	# Wykonaj funkcje w zaleznosci od wybranej przez uzytkownika opcji
 	if [ $menuOption = 1 ] 
	then
		grepOption
	elif [ $menuOption = 2 ]
	then
		sortOption
	elif [ $menuOption = 3 ]
	then
		sedOption
	fi

	# Wyswietl menu
	showMenu menuOption
done

# Zakoncz przekierowanie ze strumienia 3 do strumienia 1 (STDOUT)
exec 3>&-;
